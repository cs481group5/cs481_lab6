//
// Group 5: CJ Trujillo, Ian Altoveros, Gintas Kazlauskas, Howard Tep
// 11/17/20
// CS 481 Lab 6
// Container Transform, Fade Through, & Progress Indicators
//
// Citation: api.flutter.dev
//    images- freeimages.com
//    image dialogu: https://stackoverflow.com/questions/60047676/flutter-display-photo-or-image-as-modal-popup
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> images = ['assets/images/picture1.jpg', 'assets/images/picture2.jpg', 'assets/images/picture3.jpg', 'assets/images/picture4.jpg', 'assets/images/picture5.jpg',
    'assets/images/picture6.jpg', 'assets/images/picture7.jpg', 'assets/images/picture8.jpg', 'assets/images/picture9.jpg', 'assets/images/picture10.jpg'];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Images"),
      ),
      backgroundColor: Colors.grey,
      body: Container(
        padding: EdgeInsets.all(16),
        child: GridView.builder(
          itemCount: images.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3,
              crossAxisSpacing: 1, mainAxisSpacing: 1),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () async {
                showDialog(
                  context: context,
                  builder: (_) => Dialog(
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: 500,
                          height: 500,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: ExactAssetImage(images[index]),
                                fit: BoxFit.contain,
                              )
                          ),
                        ),
                        Container(
                          width: 100,
                          height: 100,
                          alignment: Alignment.topCenter,
                          child: Row(
                            children: [
                              IconButton(
                                icon: Icon(Icons.edit),
                              ),
                              IconButton(
                                icon: Icon(Icons.download_rounded),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
              child: Container(
                  child: Image.asset(
                    images[index],
                    fit: BoxFit.cover,
                  )
              ),
            );
          },
        ),
      ),
    );
  }
}